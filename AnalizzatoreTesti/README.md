# Esercizio: Analizzatore di Testo in Java

## Obiettivo
Sviluppare un'applicazione Java che permetta di eseguire varie operazioni di analisi e manipolazione su stringhe di testo.

## Funzionalità
L'applicazione dovrebbe supportare le seguenti operazioni su un testo fornito:

1. **Contare le Occorrenze di una Parola**: Determina quante volte una specifica parola appare nel testo.
2. **Ricerca Parola**: Verifica la presenza di una specifica parola all'interno del testo.
3. **Sostituire Parole**: Sostituisce tutte le occorrenze di una parola nel testo con un'altra.
4. **Convertire in Maiuscolo/Minuscolo**: Modifica tutte le lettere del testo in maiuscolo o in minuscolo.
5. **Contare le Parole**: Calcola il numero totale di parole presenti nel testo.
6. **Invertire il Testo**: Inverte l'ordine dei caratteri nel testo.

## Implementazione

Le funzioni sono implementate nella classe `AnalizzatoreDiTesto`, utilizzando i metodi forniti dalle stringhe in Java per manipolare e analizzare il testo.

- `contaOccorrenze` usa `indexOf` per trovare tutte le occorrenze di una parola, considerando il testo in minuscolo per ignorare le differenze di maiuscolo/minuscolo.
- `ricercaParola` sfrutta `contains` per verificare
