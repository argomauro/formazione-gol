# Presentazione sui Metodi delle Stringhe in Java

## Introduzione ai Metodi delle Stringhe
Le stringhe in Java sono oggetti che rappresentano sequenze di caratteri. La classe `String` in Java fornisce numerosi metodi per svolgere operazioni sulle stringhe, come la ricerca di sottosequenze, la comparazione, la manipolazione del testo, e altro ancora.

## Lunghezza di una Stringa
### `length()`
Ritorna la lunghezza di una stringa.

```java
String s = "Hello, World!";
System.out.println(s.length()); // 13
```

## Concatenazione di Stringhe
### `concat(String str)`
Concatena la stringa specificata alla fine di questa stringa.

```java
String s1 = "Hello, ";
String s2 = "World!";
System.out.println(s1.concat(s2)); // Hello, World!
```

## Ottenere un Carattere Specifico
### `charAt(int index)`
Ritorna il carattere alla posizione specificata.

```java
String s = "Hello";
System.out.println(s.charAt(1)); // e
```

## Sottostringhe
### `substring(int beginIndex)`, `substring(int beginIndex, int endIndex)`
Ritorna una nuova stringa che è una sottosequenza di questa stringa.

```java
String s = "Hello, World!";
System.out.println(s.substring(7)); // World!
System.out.println(s.substring(0, 5)); // Hello
```

## Comparare Stringhe
### `equals(Object anObject)`
Confronta questa stringa con l'oggetto specificato.

```java
String s1 = "Hello";
String s2 = "Hello";
String s3 = "hello";
System.out.println(s1.equals(s2)); // true
System.out.println(s1.equals(s3)); // false
```

### `equalsIgnoreCase(String anotherString)`
Confronta questa stringa con un'altra stringa, ignorando le differenze di maiuscole e minuscole.

```java
String s1 = "Hello";
String s2 = "hello";
System.out.println(s1.equalsIgnoreCase(s2)); // true
```

## Indice di Caratteri e Sottosequenze
### `indexOf(int ch)`, `indexOf(String str)`
Ritorna l'indice della prima occorrenza del carattere o della sottosequenza specificata.

```java
String s = "Hello, World!";
System.out.println(s.indexOf('o')); // 4
System.out.println(s.indexOf("World")); // 7
```

## Sostituzione di Caratteri
### `replace(char oldChar, char newChar)`
Sostituisce tutte le occorrenze del carattere specificato con un nuovo carattere.

```java
String s = "Hello, World!";
System.out.println(s.replace('o', '0')); // Hell0, W0rld!
```

## Conversione in Maiuscolo e Minuscolo
### `toUpperCase()`, `toLowerCase()`
Converte tutti i caratteri di questa stringa in maiuscolo o minuscolo.

```java
String s = "Hello, World!";
System.out.println(s.toUpperCase()); // HELLO, WORLD!
System.out.println(s.toLowerCase()); // hello, world!
```

## Rimozione degli Spazi Bianchi
### `trim()`
Rimuove gli spazi bianchi iniziali e finali.

```java
String s = "  Hello, World!  ";
System.out.println(s.trim()); // Hello, World!
```

## Conclusione
Questi sono solo alcuni dei metodi fondamentali offerti dalla classe `String` in Java per manipolare e gestire le stringhe. Esistono molti altri metodi utili che possono essere esplorati nella documentazione ufficiale di Java.
