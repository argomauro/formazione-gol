public class AnalizzatoreDiTesto {

    // Conta le occorrenze di una parola in un testo
    public static int contaOccorrenze(String testo, String parola) {
        String testoMinuscolo = testo.toLowerCase();
        String parolaMinuscolo = parola.toLowerCase();
        int conta = 0;
        int index = 0;
        
        // Ciclo fino a quando non troviamo più occorrenze
        while ((index = testoMinuscolo.indexOf(parolaMinuscolo, index)) != -1) {
            conta++;
            index += parolaMinuscolo.length();
        }
        return conta;
    }

    public static String sostituisciCarattere(String s, char daSostituire, char sostituitoCon){
        String risultato = s.replace(daSostituire, sostituitoCon);
        return risultato;
    }

    public static String estraiString(){
        String s = "UNIBAS-223";
        int indiceTrattino = s.indexOf('-');
        String risultato = s.substring(indiceTrattino+1);
        System.out.println(risultato);
        return risultato;
    }
    // Verifica se una parola è presente nel testo
    public static boolean ricercaParola(String testo, String parola) {
        //return testo.toLowerCase().contains(parola.toLowerCase());
        return testo.contains(parola);
    }

    // Sostituisce tutte le occorrenze di una parola con un'altra
    public static String sostituireParole(String testo, String target, String sostituzione) {
        return testo.replaceAll(target, sostituzione);
    }

    // Converte il testo in maiuscolo
    public static String convertiInMaiuscolo(String testo) {
        return testo.toUpperCase();
    }

    // Converte il testo in minuscolo
    public static String convertiInMinuscolo(String testo) {
        return testo.toLowerCase();
    }

    // Conta il numero di parole in un testo
    public static int contaParole(String testo) {
        String[] parole = testo.split("\\s+");
        return parole.length;
    }

    // Inverte il testo
    public static String invertiTesto(String testo) {
        return new StringBuilder(testo).reverse().toString();
    }

    public static String estrai(String s, char delimitatore){
        int indiceCarattere = s.indexOf(delimitatore);
        String sottoStringa = s.substring(indiceCarattere + 1);
        return sottoStringa;
    }

    public static boolean confrontaNomi(String s1, String s2){
        String s1Lower = s1.toLowerCase();
        String s2Lower = s2.toLowerCase();
        boolean risultato = s1Lower.equals(s2Lower);
        return risultato;
    }
    public static void main(String[] args) {
        String testo = "Hello World! Hello Java.";
        String parola = "hello";
        boolean trovato = ricercaParola(testo.toLowerCase(), parola.toLowerCase());
        String stampa = "La parola '".concat(parola)
            .concat("' è presente nel testo? ")
            .concat(String.valueOf(trovato));

        //System.out.println(stampa);
        
        //String sostituisci = sostituisciCarattere("milano",'m','l');
        //System.out.println("Estrai: " + estraiString());

        String sottoStringa = estrai("PRIMA-1345", '-');
        System.out.println("Sottostringa: " + sottoStringa);

        String s1 = "Mimmo";
        String s2 = "miMMo";
        boolean risultatoConfronto = confrontaNomi(s1,s2);
        System.out.println("SONO UGUALI: "+risultatoConfronto);

        /**
        System.out.println("Numero di occorrenze della parola '" + parola + "': " + contaOccorrenze(testo, parola));
        System.out.println("Testo con 'Hello' sostituito con 'Hi': " + sostituireParole(testo, "Hello", "Hi"));
        System.out.println("Testo in maiuscolo: " + convertiInMaiuscolo(testo));
        System.out.println("Testo in minuscolo: " + convertiInMinuscolo(testo));
        System.out.println("Numero di parole nel testo: " + contaParole(testo));
        System.out.println("Testo invertito: " + invertiTesto(testo));
         */
    }
}
