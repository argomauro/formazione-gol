import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import it.gol.Cane;

public class TestCane {

    @Test
    public void testCalcolaPericolosita(){
        Cane c = new Cane("mimmo", "Tedesco", 300);
        Cane c2 = new Cane("mimmo", "Tedesco", 20);

        assertEquals(1, c.calcolaPericolosita());
        assertEquals(0, c2.calcolaPericolosita());

    }

    @Test
    public void testCalcolaPericolositaBooleana(){
        Cane c = new Cane("mimmo", "Tedesco", 300);
        Cane c2 = new Cane("mimmo", "Tedesco", 20);
        assertTrue(c.calcolaPericolositaBooleana());
        assertFalse(c2.calcolaPericolositaBooleana());
    }

    @Test
    public void testCalcoloListaCani(){
        List<Cane> listaCani = new ArrayList<Cane>();
        Cane c = new Cane("mimmo", "Tedesco", 300);
        Cane c2 = new Cane("mimmo", "Tedesco", 20);
        listaCani.add(c);
        listaCani.add(c2);
        assertEquals(2, listaCani.size());
        
    }

    @Test
    public void testCreazioneOggetto(){
        Cane c = new Cane("mimmo", "Tedesco", 300);
        Cane c2 = new Cane("mimmo", "Tedesco", 20);

        assertEquals("mimmo", c.getNome());
    }
}
