package it.gol;

public class Cane {
    private String nome;
    private String razza;
    private int lunghezza;
    
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getRazza() {
        return razza;
    }
    public void setRazza(String razza) {
        this.razza = razza;
    }
    public Cane(String nome, String razza, int lunghezza) {
        this.nome = nome;
        this.razza = razza;
        this.lunghezza = lunghezza;
    }
    public Cane() {
    }
    @Override
    public String toString() {
        return "Cane [nome=" + nome + ", razza=" + razza + ", lunghezza="+lunghezza+" ]";
    }
    public int getLunghezza() {
        return lunghezza;
    }
    public void setLunghezza(int lunghezza) {
        this.lunghezza = lunghezza;
    }

    public int calcolaPericolosita(){
        int pericolosita = 0;
        if (lunghezza > 100)
            pericolosita = 1;

        return pericolosita;
    }
    
    public boolean calcolaPericolositaBooleana(){
        boolean pericolosita = false;
        if (lunghezza > 100)
            pericolosita = true;

        return pericolosita;
    }
    
}
