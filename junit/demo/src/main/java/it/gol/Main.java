package it.gol;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> lista = new ArrayList<String>();
        lista.add("UNO");
        lista.add("DUE");
        Cane c = null;
        try {
            System.out.println(lista.get(2)); // Accesso a un indice non esistente
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Errore. Hai provato ad accedere ad un oggetto che non esiste. Numero di ggetti presenti in lista " + lista.size());
        }
        try {
            c.getNome();
        } catch (NullPointerException e) {
            System.err.println("Errore: NULL.");
            c = new Cane();
        }
        c.getNome();
        System.out.println("Hello world!");
        dividi(5,0);
        String numeroStr = "jgjgjhgjh";
        try {
            int numero = Integer.parseInt(numeroStr); // Conversione fallita
            System.out.println(numero);
        } catch (NumberFormatException e) {
            System.err.println("Errore: la stringa non è un numero valido.");
        }
        

    }

    public static int dividi(int numeratore, int denominatore) {
        try {
            return numeratore / denominatore;
        } catch (ArithmeticException e) {
            System.err.println("Errore: tentativo di divisione per zero.");
            return 0; // Restituiamo un valore di default o gestiamo l'errore in altro modo
        }
    }
}