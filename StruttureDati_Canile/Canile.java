import java.util.*;

public class Canile {
    private static final int numMax = 10;
    private Map<String,Cane> mappaCani = new HashMap<>();
    private Map<String,Addestratore> mappaAddestratori = new HashMap<>();

        
    public Map<String, Addestratore> getMappaAddestratori() {
        return mappaAddestratori;
    }

    public void setMappaAddestratori(Map<String, Addestratore> mappaAddestratori) {
        this.mappaAddestratori = mappaAddestratori;
    }

    public Map<String, Cane> getMappaCani() {
        return mappaCani;
    }
    public void setMappaCani(Map<String, Cane> mappaCani) {
        this.mappaCani = mappaCani;
    }

    public void aggiungiCane(Cane cane){
        if(controllaCapienza()){
            this.mappaCani.put(cane.getIdentificativo(), cane);
            System.out.println("Cane aggiunto: "+ cane);
        }
        else{
            System.out.println("Mi dispiace ci sono troppi cani!!!");
        }
    }

    public void aggiungiAddestratore(Addestratore addestratore){
        this.mappaAddestratori.put(addestratore.getCodiceFiscale(), addestratore);
        System.out.println("Addestratore aggiunto: "+ addestratore);
        
    }

    private boolean controllaCapienza(){
        int caniPresenti = this.mappaCani.size();
        if(caniPresenti < numMax){
            System.out.println("Puoi inserire il cane!");
            return true;
        }else{
            System.out.println("Non puoi inserire il cane!");
            return false;
        }


    }
    public boolean rimuoviCane(String identificativo){
        boolean rimosso = false;
        Cane caneRimosso = this.mappaCani.remove(identificativo);
        if(caneRimosso != null)
            rimosso = true;
        return rimosso;
    }

    public boolean rimuoviAddestratore(String codiceFiscale){
        boolean rimosso = false;
        Addestratore addestratoreRimosso = this.mappaAddestratori.remove(codiceFiscale);
        if(addestratoreRimosso != null)
            rimosso = true;
        return rimosso;
    }

    public void stampaCanNonAffidamento(){
        System.out.println("Stampo cani NON- INIZIO");
        for(Map.Entry<String,Cane> elemento : mappaCani.entrySet()){
            Cane c = elemento.getValue();
            if(!c.isInAffidamento())
                System.out.println(c);
        }
        System.out.println("Stampo cani NON - FINE");
    }

    public void stampaCaninAffidamento(){
        System.out.println("Stampo cani - INIZIO");
        for(Map.Entry<String,Cane> elemento : mappaCani.entrySet()){
            Cane c = elemento.getValue();
            if(c.isInAffidamento())
                System.out.println(c);
        }
        System.out.println("Stampo cani - FINE");
    }

    public void rimuoviCaneSemplice(String identificativo){
        this.mappaCani.remove(identificativo);
    }

}
