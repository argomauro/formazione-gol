public class Addestratore {
    private String nome;
    private String cognome;
    private int eta;
    private String codiceFiscale;
    
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public int getEta() {
        return eta;
    }
    public void setEta(int eta) {
        this.eta = eta;
    }
    public String getCodiceFiscale() {
        return codiceFiscale;
    }
    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }
    public Addestratore(String nome, String cognome, int eta, String codiceFiscale) {
        this.nome = nome;
        this.cognome = cognome;
        this.eta = eta;
        this.codiceFiscale = codiceFiscale;
    }
    public Addestratore() {
    }
    @Override
    public String toString() {
        return "Addestratore [nome=" + nome + ", cognome=" + cognome + ", eta=" + eta + ", codiceFiscale="
                + codiceFiscale + "]";
    }


    


}
