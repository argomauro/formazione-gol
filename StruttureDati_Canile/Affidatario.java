import java.util.*;

public class Affidatario {
private String nome;
private String congome;
private int eta;
private String codiceFiscale;
private boolean patentitno;
private String dataPatentino;
List<Cane> listaCani = new ArrayList<Cane>();


public Affidatario() {

}

public Affidatario(String nome, String congome, int eta, String codiceFiscale, boolean patentitno,
        String dataPatentino) {
    this.nome = nome;
    this.congome = congome;
    this.eta = eta;
    this.codiceFiscale = codiceFiscale;
    this.patentitno = patentitno;
    this.dataPatentino = dataPatentino;
}
public String getNome() {
    return nome;
}
public void setNome(String nome) {
    this.nome = nome;
}
public String getCongome() {
    return congome;
}
public void setCongome(String congome) {
    this.congome = congome;
}
public int getEta() {
    return eta;
}
public void setEta(int eta) {
    this.eta = eta;
}
public String getCodiceFiscale() {
    return codiceFiscale;
}
public void setCodiceFiscale(String codiceFiscale) {
    this.codiceFiscale = codiceFiscale;
}
public boolean isPatentitno() {
    return patentitno;
}
public void setPatentitno(boolean patentitno) {
    this.patentitno = patentitno;
}
public String getDataPatentino() {
    return dataPatentino;
}
public void setDataPatentino(String dataPatentino) {
    this.dataPatentino = dataPatentino;
}
public List<Cane> getListaCani() {
    return listaCani;
}
public void setListaCani(List<Cane> listaCani) {
    this.listaCani = listaCani;
}

public boolean aggiungiCane(Cane cane){
    boolean inserito = this.listaCani.add(cane);
    return inserito;
}

public boolean rimuoviCane(Cane cane){
    boolean cancellato = this.listaCani.remove(cane);
    return cancellato;
}

public void stampaCani(){
    System.out.println("Cani di " + this.nome);
    for(Cane cane:this.listaCani){
        System.out.println(cane);
    }
}


    
}