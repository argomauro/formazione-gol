public class Cane {
    private String identificativo;
    private String nome;
    private String razza;
    private int lunghezza;
    private boolean inAffidamento = false;
    Addestratore addestratore;

        
    public Addestratore getAddestratore() {
        return addestratore;
    }

    public void setAddestratore(Addestratore addestratore) {
        this.addestratore = addestratore;
    }

    public String getIdentificativo() {
        return identificativo;
    }

    public void setIdentificativo(String identificativo) {
        this.identificativo = identificativo;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getRazza() {
        return razza;
    }
    public void setRazza(String razza) {
        this.razza = razza;
    }
    public Cane(String identificativo, String nome, String razza, int lunghezza) {
        this.identificativo = identificativo;
        this.nome = nome;
        this.razza = razza;
        this.lunghezza = lunghezza;
    }
    public Cane() {
    }
    
    @Override
    public String toString() {
        String stampaCane = "Cane [identificativo=" + identificativo + ", nome=" + nome + ", razza=" + razza + ", lunghezza="
        + lunghezza + ", inAffidamento=" + inAffidamento + "] \n";
        String stampaAddestratore = "";
        if(this.addestratore != null){
            stampaAddestratore = "Addestrato da = " + addestratore.getNome();
        }
        return stampaCane.concat(stampaAddestratore);
    }

    public int getLunghezza() {
        return lunghezza;
    }
    public void setLunghezza(int lunghezza) {
        this.lunghezza = lunghezza;
    }

    public boolean isInAffidamento() {
        return inAffidamento;
    }

    public void setInAffidamento(boolean inAffidamento) {
        this.inAffidamento = inAffidamento;
    }
    
    
}
