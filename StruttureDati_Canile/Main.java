import java.util.*;


public class Main {

    public static void main(String[] args) {
        Addestratore a1 = new Addestratore("Maurizio", "Argoneto", 37, "RGNMRZ81S11M1O1P");
        Addestratore a2 = new Addestratore("Michele", "Filippo", 37, "LLKJDJDJDJ889KDJD");

        Cane c1 = new Cane("CANE-001","pippo","bassotto",5);
        c1.setInAffidamento(true);
        c1.setAddestratore(a1);
        System.out.println(c1);
        Cane c2 = new Cane("CANE-002","michele","pitbull",2);
        Cane c3 = new Cane("CANE-003","giovanni","maremmano",2);
        System.out.println(c2);

        
        Canile canile = new Canile();
        //INSERISCO I CANI NEL CANILE
        canile.aggiungiCane(c1);
        canile.aggiungiCane(c2);
        canile.aggiungiCane(c3);
        //INSERISCO GLI ADDESTRATORI NEL CANILE
        canile.aggiungiAddestratore(a1);
        canile.aggiungiAddestratore(a2);

        canile.stampaCaninAffidamento();
        canile.stampaCanNonAffidamento();

        Affidatario affidatario1 = new Affidatario("Maurizio","Argoneto" ,43, "RGNMRZ81s11m109p", true, "12/12/2023");
        Affidatario affidatario2 = new Affidatario("Michele","Palermo" ,25, "KJKJJJKHJUUOOI99", false, null);
        affidatario1.aggiungiCane(c1);
        affidatario2.aggiungiCane(c2);
        affidatario1.stampaCani();
        affidatario2.stampaCani();




    }

    public static void esempioHashSet() {
        boolean inserita = false;
        Set<String> insieme = new HashSet<>();
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Inserisci il valore " + i + "--> ");
            String s = input.nextLine();
            if (!insieme.add(s))
                System.out.println(" Parola ripetuta : " + s);

        }
        System.out.println("Vuoi verificare se esiste un elemento? Inserisci --> ");
        String verifica = input.nextLine();
        boolean isPresente = insieme.contains(verifica);
        System.out.println("Il tuo valore " + verifica + " esiste: " + isPresente);
        int numeroElementi = insieme.size();
        System.out.println("Adesso leggiamo i valori\n\n ");
        System.out.println("Adesso ci sono " + numeroElementi + " valori\n\n ");
        insieme.clear();
        for (String frutto : insieme) {
            System.out.println(frutto);
        }
    }

    public static Map<String,Cane> esempioHashMap() {
        Map<String,Cane> mappaCani = new HashMap<>();
        Cane c1 = new Cane("C1","pippo","bassotto",5);
        Cane c2 = new Cane("C1","michele","pitbull",2);
        mappaCani.put(c1.getIdentificativo(), c1);
        mappaCani.put(c1.getIdentificativo(), c2);
        return mappaCani;
    }

    public static void esempioArrayList() {
        List<Cane> lista = new ArrayList<>();
        Cane c1 = new Cane("C1","pippo","bassotto",5);
        Cane c2 = new Cane("C2","michele","pitbull",2);

        lista.add(c1);
        lista.add(c2);

        // Iterazione sulla lista
        for (Cane cane : lista) {
            if(cane.getLunghezza() > 3)
                System.out.println("Benvenuto "+ cane.getNome());
            else
                System.out.println("Mi dispiace non puoi entrare "+ cane.getNome());

            //System.out.println(cane);
        }
        System.out.println("Stampa: " + lista.get(0));
    }
}
