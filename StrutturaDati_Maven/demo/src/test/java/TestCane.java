import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import it.gol.strutturedati.Cane;

public class TestCane {

    @Test
    public void testCane(){
        Cane c = new Cane("mimmo", "Tedesco", 300);

        String nomeCane = "mimmo";
        assertEquals(nomeCane, c.getNome());
    }
}
