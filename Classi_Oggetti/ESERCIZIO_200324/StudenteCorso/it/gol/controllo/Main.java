package it.gol.controllo;

import it.gol.modello.Aula;
import it.gol.modello.Corso;
import it.gol.modello.Studente;

public class Main {
    public static void main(String[] args) {
        Aula aulaCorsoJava = new Aula("12","I°");
        Corso corsoJava = new Corso("Programmazione Java"
        ,"JVM33", 12);
        corsoJava.setAula(aulaCorsoJava);
        Studente studente1 = new Studente("Maurizio","Argoneto", 123, corsoJava);
        Studente studente2 = new Studente("Domenico","Bifolco", 345);
        studente2.setCorsoDiStudio(corsoJava);
        System.out.println("Corso che segue "+
        studente1.getCognome()+": \n  "+ studente1.getCorsoDiStudio().getNomeCorso());
        System.out.println("Corso che segue "+
        studente2.getCognome()+": \n  "+ studente2.getCorsoDiStudio().getNomeCorso());
        studente1.stampaAula();
    }   
}
