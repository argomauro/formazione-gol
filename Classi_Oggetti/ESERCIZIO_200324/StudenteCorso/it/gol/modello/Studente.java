package it.gol.modello;

public class Studente {
    private String nome;
    private String cognome;
    private int matricola;
    private Corso corsoDiStudio;
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public int getMatricola() {
        return matricola;
    }
    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }
    public Corso getCorsoDiStudio() {
        return corsoDiStudio;
    }
    public void setCorsoDiStudio(Corso corsoDiStudio) {
        this.corsoDiStudio = corsoDiStudio;
    }
    public Studente() {
    }
    public Studente(String nome, String cognome, int matricola, Corso corsoDiStudio) {
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
        this.corsoDiStudio = corsoDiStudio;
    }
    public Studente(String nome, String cognome, int matricola) {
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
    }
    @Override
    public String toString() {
        return "Studente [nome=" + nome + ", cognome=" + cognome + ", matricola=" + matricola + ", corsoDiStudio="
                + corsoDiStudio + "]";
    }

    public void stampaAula(){
        System.out.println("Studente "+this.nome 
        + ", segue il corso "
        + this.corsoDiStudio.getNomeCorso() 
        + ", in aula numero:"
        + this.corsoDiStudio.getAula().getNumero());
    }
    
}
