package it.gol.modello;

public class Aula {
    private String numero;
    private String piano;
    public String getNumero() {
        return numero;
    }
    public void setNumero(String numero) {
        this.numero = numero;
    }
    public String getPiano() {
        return piano;
    }
    public void setPiano(String piano) {
        this.piano = piano;
    }
    public Aula() {
    }
    public Aula(String numero, String piano) {
        this.numero = numero;
        this.piano = piano;
    }
    @Override
    public String toString() {
        return "Aula [numero=" + numero + ", piano=" + piano + "]";
    }
    
}
