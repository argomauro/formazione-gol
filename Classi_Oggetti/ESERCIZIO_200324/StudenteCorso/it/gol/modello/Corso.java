package it.gol.modello;

public class Corso {
    private String nomeCorso;
    private String codiceCorso;
    private int crediti;
    private Aula aula;
    
    public String getNomeCorso() {
        return nomeCorso;
    }
    public void setNomeCorso(String nomeCorso) {
        this.nomeCorso = nomeCorso;
    }
    public String getCodiceCorso() {
        return codiceCorso;
    }
    public void setCodiceCorso(String codiceCorso) {
        this.codiceCorso = codiceCorso;
    }
    public int getCrediti() {
        return crediti;
    }
    public void setCrediti(int crediti) {
        this.crediti = crediti;
    }
    public Corso(String nomeCorso, String codiceCorso, int crediti) {
        this.nomeCorso = nomeCorso;
        this.codiceCorso = codiceCorso;
        this.crediti = crediti;
    }
    public Corso() {
    }
    @Override
    public String toString() {
        return "Corso [nomeCorso=" + nomeCorso + ", codiceCorso=" + codiceCorso + ", crediti=" + crediti + "]";
    }
    public Aula getAula() {
        return aula;
    }
    public void setAula(Aula aula) {
        this.aula = aula;
    }
         
}
