package it.gol.modello;

public class Film {
    private String titolo;
    private int anno;
    private int durata;
    private String genere;
    private String descrizione;
    private Attore attore;

    
    public Attore getAttore() {
        return attore;
    }
    public void setAttore(Attore attore) {
        this.attore = attore;
    }
    public Film() {
    }
    
    public Film(String titolo, int anno, int durata, String genere, String descrizione, Attore attore) {
        this.titolo = titolo;
        this.anno = anno;
        this.durata = durata;
        this.genere = genere;
        this.descrizione = descrizione;
        this.attore = attore;
    }
    public Film(String titolo, int anno, int durata, String genere, String descrizione) {
        this.titolo = titolo;
        this.anno = anno;
        this.durata = durata;
        this.genere = genere;
        this.descrizione = descrizione;
    }
    public String getTitolo() {
        return titolo;
    }
    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }
    public int getAnno() {
        return anno;
    }
    public void setAnno(int anno) {
        this.anno = anno;
    }
    public int getDurata() {
        return durata;
    }
    public void setDurata(int durata) {
        this.durata = durata;
    }
    public String getGenere() {
        return genere;
    }
    public void setGenere(String genere) {
        this.genere = genere;
    }
    public String getDescrizione() {
        return descrizione;
    }
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
    @Override
    public String toString() {
        return "Film [titolo=" + titolo + ", anno=" + anno + ", durata=" + durata + ", genere=" + genere
                + ", descrizione=" + descrizione + "]";
    }

    
    
}
