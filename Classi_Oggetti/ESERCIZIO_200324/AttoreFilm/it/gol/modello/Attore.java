package it.gol.modello;

public class Attore {
    private String nome;
    private String sesso;
    private int anno;
    
    public Attore() {
    }

    public Attore(String nome, String sesso, int anno) {
        this.nome = nome;
        this.sesso = sesso;
        this.anno = anno;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getSesso() {
        return sesso;
    }
    public void setSesso(String sesso) {
        this.sesso = sesso;
    }
    public int getAnno() {
        return anno;
    }
    public void setAnno(int anno) {
        this.anno = anno;
    }

    @Override
    public String toString() {
        return "Attore [nome=" + nome + ", sesso=" + sesso + ", anno=" + anno + "]";
    }
     
}
