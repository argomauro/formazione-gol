package it.gol.controllo;

import it.gol.modello.Attore;
import it.gol.modello.Film;

public class Main {
    public static void main(String[] args) {
        Attore attore1 = new Attore("Maurizio Argoneto", "M", 1981);
        //System.out.println(attore1.toString());
        Film film1 = new Film("Nubi dal cielo", 2019, 180, "Romantico", 
        "Un film che parla di due persone", 
        attore1);
        System.out.println("FILM 1 "+ film1.getTitolo()+": \n"+film1.getAttore().toString());
        Film film2 = new Film("Terra bruciata", 2021, 60, "Horror", 
        "Un film horror", 
        attore1);
        System.out.println("FILM 2 "+ film2.getTitolo()+": \n"+film2.getAttore().toString());

    }   
}
