import java.util.*;


public class Main {

    public static void main(String[] args) {
        Cane c1 = new Cane("pippo","bassotto",5);
        Cane c2 = new Cane("michele","pitbull",2);
        Cane c3 = new Cane("giovanni","maremmano",2);

        Canile canile = new Canile();
        canile.aggiungiCane("CANE-001", c1);
        canile.aggiungiCane("CANE-002", c2);
        canile.aggiungiCane("CANE-003", c3);


    }

    public static void esempioHashSet() {
        boolean inserita = false;
        Set<String> insieme = new HashSet<>();
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Inserisci il valore " + i + "--> ");
            String s = input.nextLine();
            if (!insieme.add(s))
                System.out.println(" Parola ripetuta : " + s);

        }
        System.out.println("Vuoi verificare se esiste un elemento? Inserisci --> ");
        String verifica = input.nextLine();
        boolean isPresente = insieme.contains(verifica);
        System.out.println("Il tuo valore " + verifica + " esiste: " + isPresente);
        int numeroElementi = insieme.size();
        System.out.println("Adesso leggiamo i valori\n\n ");
        System.out.println("Adesso ci sono " + numeroElementi + " valori\n\n ");
        insieme.clear();
        for (String frutto : insieme) {
            System.out.println(frutto);
        }
    }

    public static Map<String,Cane> esempioHashMap() {
        Map<String,Cane> mappaCani = new HashMap<>();
        Cane c1 = new Cane("pippo","bassotto",5);
        Cane c2 = new Cane("michele","pitbull",2);
        mappaCani.put("CANE-001", c1);
        mappaCani.put("CANE-002", c2);
        return mappaCani;
    }

    public static void esempioArrayList() {
        List<Cane> lista = new ArrayList<>();
        Cane c1 = new Cane("pippo","bassotto",5);
        Cane c2 = new Cane("michele","pitbull",2);

        lista.add(c1);
        lista.add(c2);

        // Iterazione sulla lista
        for (Cane cane : lista) {
            if(cane.getLunghezza() > 3)
                System.out.println("Benvenuto "+ cane.getNome());
            else
                System.out.println("Mi dispiace non puoi entrare "+ cane.getNome());

            //System.out.println(cane);
        }
        System.out.println("Stampa: " + lista.get(0));
    }
}
