# Introduzione alle Strutture Dati in Java

## Slide 1: Introduzione alle Strutture Dati in Java

Le strutture dati sono fondamentali per organizzare e memorizzare dati in modo efficiente. La scelta della struttura dati giusta può significativamente migliorare le prestazioni di un programma. In questa presentazione, esploreremo le strutture dati fondamentali in Java: `List`, `LinkedList`, `Map`, `HashMap`, e `Set`.

## Slide 2: List in Java

### Descrizione

La `List` in Java è un'interfaccia che rappresenta un elenco ordinato. La classe `ArrayList` è una delle implementazioni più utilizzate.

### Vantaggi

- Accesso sequenziale, ricerca, ordinamento e iterazione efficienti.

### Svantaggi

- Prestazioni ridotte per grandi volumi di dati durante l'aggiunta o la rimozione.

### Esempio di Codice

```java
List<String> lista = new ArrayList<>();
lista.add("Mela");
lista.add("Banana");
lista.add("Arancia");
// Iterazione sulla lista
for(String frutto : lista) {
    System.out.println(frutto);
}
```

## Slide 2b: Metodi Principali di List

- `add(E e)`: Aggiunge un elemento alla lista.
- `get(int index)`: Ritorna l'elemento alla posizione specificata.
- `remove(int index)`: Rimuove l'elemento alla posizione specificata.
- `size()`: Ritorna il numero di elementi nella lista.
- `clear()`: Rimuove tutti gli elementi dalla lista.

## Slide 3: LinkedList in Java

### Descrizione

`LinkedList` offre metodi specifici per l'aggiunta, la rimozione e l'accesso agli elementi all'inizio e alla fine della lista.

### Vantaggi

- Inserimento e cancellazione degli elementi efficienti.

### Svantaggi

- Accesso sequenziale più lento rispetto alle ArrayList.

### Esempio di Codice

```java
List<String> listaCollegata = new LinkedList<>();
listaCollegata.add("Mela");
listaCollegata.addFirst("Banana");
listaCollegata.addLast("Arancia");
// Rimozione del primo elemento
listaCollegata.removeFirst();
```

## Slide 3b: Metodi Principali di LinkedList

- `addFirst(E e)`: Inserisce l'elemento all'inizio della lista.
- `addLast(E e)`: Aggiunge l'elemento alla fine della lista.
- `removeFirst()`: Rimuove e ritorna il primo elemento della lista.
- `removeLast()`: Rimuove e ritorna l'ultimo elemento della lista.
- `getFirst()`: Ritorna il primo elemento della lista.
- `getLast()`: Ritorna l'ultimo elemento della lista.

## Slide 4: Map in Java

### Descrizione

La `Map` in Java è un'interfaccia che rappresenta una mappatura tra una chiave univoca e un valore.

### Vantaggi

- Ricerca, inserimento, e rimozione efficienti basati su chiave.

### Svantaggi

- Non mantiene l'ordine degli elementi.

### Esempio di Codice

```java
Map<String, Integer> mappa = new HashMap<>();
mappa.put("Mela", 1);
mappa.put("Banana", 2);
mappa.put("Arancia", 3);
// Iterazione sulla mappa
for(Map.Entry<String, Integer> entry : mappa.entrySet()) {
    System.out.println(entry.getKey() + ": " + entry.getValue());
}
```

## Slide 4b: Metodi Principali di Map

- `put(K key, V value)`: Associa il valore specificato con la chiave specificata nella mappa.
- `get(Object key)`: Ritorna il valore al quale la chiave specificata è mappata.
- `remove(Object key)`: Rimuove l'associazione per una chiave, se presente.
- `keySet()`: Ritorna un set di tutte le chiavi contenute in questa mappa.
- `values()`: Ritorna una collezione di tutti i valori presenti nella mappa.

## Slide 5: HashMap in Java

### Descrizione

`HashMap` è un'implementazione dell'interfaccia Map che utilizza una tabella hash.

### Vantaggi

- Accesso molto veloce ai dati.

### Svantaggi

- Non è sincronizzata e non garantisce l'ordine degli elementi durante l'iterazione.

### Esempio di Codice

Simile alla slide precedente, focalizzato su `HashMap`.

## Slide 5b: Metodi Principali di HashMap

- Gli stessi metodi di `Map`, con l'aggiunta di:
- `putIfAbsent(K key, V value)`: Inserisce il valore specificato solo se non è già associato a una chiave.
- `replace(K key, V value)`: Sostituisce il valore associato alla chiave specificata solo se la chiave è già presente.

## Slide 6: Set in Java

### Descrizione

La `Set` in Java è un'interfaccia che rappresenta una collezione che non contiene duplicati.

### Vantaggi

- Eliminazione efficace dei duplicati, ricerca veloce.

### Svantaggi

- Non mantiene l'ordine degli elementi, a meno che non si utilizzi un `LinkedHashSet`.

### Esempio di Codice

```java
Set<String> insieme = new HashSet<>();
insieme.add("Mela");
insieme.add("Banana");
insieme.add("Arancia");
insieme.add("Mela"); // Questo elemento non sarà aggiunto
// Iterazione sul set
for(String frutto : insieme) {
    System.out.println(frutto);
}
```

## Slide 6b: Metodi Principali di Set

- `add(E e)`: Aggiunge l'elemento specificato al set se non è già presente.
- `remove(Object o)`: Rimuove l'occorrenza specificata dall'insieme, se presente.
- `contains(Object o)`: Ritorna true se l'insieme contiene l'elemento specificato.
- `size()`: Ritorna il numero di elementi nell'insieme.
- `clear()`: Rimuove tutti gli elementi dall'insieme.
```
