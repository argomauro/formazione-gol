import java.util.*;

public class Canile {
    private static final int numMax = 1;
    private Map<String,Cane> mappaCani;

    
    public Canile() {
        mappaCani = new HashMap<>();
    }

    public Map<String, Cane> getMappaCani() {
        return mappaCani;
    }
    public void setMappaCani(Map<String, Cane> mappaCani) {
        this.mappaCani = mappaCani;
    }

    public void aggiungiCane(String identificativo, Cane cane){
        if(controllaCapienza()){
            this.mappaCani.put(identificativo, cane);
            System.out.println("Cane aggiunto: "+ cane);
        }
        else{
            System.out.println("Mi dispiace ci sono troppi cani!!!");
        }
    }

    private boolean controllaCapienza(){
        int caniPresenti = this.mappaCani.size();
        if(caniPresenti < numMax){
            System.out.println("Puoi inserire il cane!");
            return true;
        }else{
            System.out.println("Non puoi inserire il cane!");
            return false;
        }


    }
    public boolean rimuoviCane(String identificativo){
        boolean rimosso = false;
        Cane caneRimosso = this.mappaCani.remove(identificativo);
        if(caneRimosso != null)
            rimosso = true;
        return rimosso;
    }

    public void rimuoviCaneSemplice(String identificativo){
        this.mappaCani.remove(identificativo);
    }

}
