package it.gol.controllo;

import it.gol.modello.*;

public class Main {
    public static void main(String[] args) {
        // Creazione dell'istanza della classe SistemaBiblioteca
        SistemaBiblioteca biblioteca = new SistemaBiblioteca();

        // Creazione e aggiunta di libri alla biblioteca
        Libro libro1 = new Libro("Il Nome della Rosa", "Umberto Eco", "Narrativa");
        Libro libro2 = new Libro("La Coscienza di Zeno", "Italo Svevo", "Narrativa");
        Libro libro3 = new Libro("Fondazione", "Isaac Asimov", "Fantascienza");

        biblioteca.aggiungiLibro(libro1);
        biblioteca.aggiungiLibro(libro2);
        biblioteca.aggiungiLibro(libro3);

        // Presta il libro1 a un utente
        biblioteca.prestaLibro("user123", libro1);

        // Stampa dei libri prestati a "user123"
        System.out.println("Libri prestati a user123: " + biblioteca.getLibriUtente("user123"));

        // Stampa degli autori unici
        biblioteca.stampaAutoriUnici();

        // Stampa delle categorie disponibili
        System.out.println("Categorie disponibili:");
        for (String categoria : biblioteca.getCategorie()) {
            System.out.println("- " + categoria);
        }

        // Aggiunta di un nuovo libro e aggiornamento del set di autori
        Libro libro4 = new Libro("Il secondo sesso", "Simone de Beauvoir", "Saggistica");
        biblioteca.aggiungiLibro(libro4);

        // Stampa aggiornata degli autori unici dopo l'aggiunta di un nuovo libro
        System.out.println("Autori aggiornati in biblioteca:");
        biblioteca.stampaAutoriUnici();
    }
}

