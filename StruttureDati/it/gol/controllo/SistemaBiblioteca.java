package it.gol.controllo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import it.gol.modello.Libro;

public class SistemaBiblioteca {
    // Lista di tutti i libri disponibili in biblioteca
    private ArrayList<Libro> libriDisponibili = new ArrayList<>();
    // Mappa per tenere traccia dei libri prestati agli utenti (ID utente -> Lista di libri)
    private HashMap<String, ArrayList<Libro>> libriPrestati = new HashMap<>();
    // Set per memorizzare un insieme unico di autori presenti nella biblioteca
    private Set<String> autori = new HashSet<>();
    // Array per categorizzare i libri (dimensione fissa)
    private String[] categorie = {"Narrativa", "Saggistica", "Poesia"};

    // Metodo per aggiungere un libro alla lista dei libri disponibili e all'insieme degli autori
    public void aggiungiLibro(Libro libro) {
        libriDisponibili.add(libro);
        autori.add(libro.getAutore());
    }

    // Metodo per prestare un libro a un utente (rimuove il libro dai disponibili e lo aggiunge alla lista dell'utente)
    public void prestaLibro(String userId, Libro libro) {
        libriDisponibili.remove(libro);
        libriPrestati.computeIfAbsent(userId, k -> new ArrayList<>()).add(libro);
    }

    // Metodo per ottenere le categorie di libri
    public String[] getCategorie() {
        return categorie;
    }

    // Metodo per ottenere la lista dei libri prestati a un determinato utente
    public ArrayList<Libro> getLibriUtente(String userId) {
        return libriPrestati.getOrDefault(userId, new ArrayList<>());
    }

    // Metodo per stampare tutti gli autori unici presenti nella biblioteca
    public void stampaAutoriUnici() {
        System.out.println("Autori in biblioteca: " + autori);
    }

    // Qui potresti aggiungere altri metodi utili, come la ricerca di libri per titolo o autore
}

