package it.gol.modello;

public class Libro {
    // Attributi della classe Libro
    private String titolo;
    private String autore;
    private String categoria;

    // Costruttore della classe Libro
    public Libro(String titolo, String autore, String categoria) {
        this.titolo = titolo;
        this.autore = autore;
        this.categoria = categoria;
    }

    // Metodi getter per accedere agli attributi privati
    public String getTitolo() { return titolo; }
    public String getAutore() { return autore; }
    public String getCategoria() { return categoria; }

    // Override del metodo toString per stampare informazioni del libro in modo leggibile
    @Override
    public String toString() {
        return "Libro{" +
                "titolo='" + titolo + '\'' +
                ", autore='" + autore + '\'' +
                ", categoria='" + categoria + '\'' +
                '}';
    }
}
