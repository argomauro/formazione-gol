# Esercizio: Sistema di Gestione Biblioteca in Java

## Obiettivo
Lo scopo dell'esercizio è implementare un semplice sistema per gestire una biblioteca, utilizzando le principali strutture dati di Java come array, ArrayList, HashMap e Set. Questo sistema permetterà di tenere traccia dei libri disponibili in biblioteca, di memorizzare i libri prestati agli utenti e di mantenere un insieme unico di autori.

## Strutture Dati Utilizzate

- **Array**: Utilizzato per memorizzare una lista statica di categorie di libri.
- **ArrayList**: Per tenere traccia dinamica dei libri disponibili in biblioteca.
- **HashMap**: Per associare ogni utente (ID utente) con la lista dei libri che ha in prestito.
- **Set**: Per mantenere un insieme unico di autori presenti nella biblioteca.
