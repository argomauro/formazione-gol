package it.gol.controllo;

import java.util.List;

import it.gol.modello.*;

public class Main {
    public static void main(String[] args) {
        
        Garage garage = new Garage();
        List<ABSVeicolo> listaVeicoli = garage.generaParcoVeicoli();
        for(ABSVeicolo veicolo: listaVeicoli){
            veicolo.mostraVelocitaMassima();
            if (veicolo instanceof Automobile) {
                System.out.println("Sono una Automobile");
            }
    
            int postiDisponibili = veicolo.postiDisponibili();
            System.out.println("Postidisponibili su  "+veicolo.getModello() + ": "+ postiDisponibili);
            veicolo.calcolaPercorso("Roma");
        }

        
    }
}
