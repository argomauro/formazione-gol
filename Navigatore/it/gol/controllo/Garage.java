package it.gol.controllo;

import java.util.ArrayList;
import java.util.List;

import it.gol.modello.ABSVeicolo;
import it.gol.modello.Automobile;
import it.gol.modello.Moto;

public class Garage {
    
    public List<ABSVeicolo> generaParcoVeicoli(){
        List<ABSVeicolo> listaVeicoli = new ArrayList<ABSVeicolo>();
        Automobile mezzo1 = new Automobile("Tesla", 1000);
        Moto mezzo2 = new Moto("Yamaha XMAX",300 );
        Automobile mezzo3 = new Automobile("Renault", 1000);
        Moto mezzo4 = new Moto("Honda CB",100 );
        Automobile mezzo5 = new Automobile("Jeep", 1000);
        Moto mezzo6 = new Moto("PIAGGIO Beverly",200 );
        mezzo6.setSideCar(true);
        listaVeicoli.add(mezzo1);
        listaVeicoli.add(mezzo2);
        listaVeicoli.add(mezzo3);
        listaVeicoli.add(mezzo4);
        listaVeicoli.add(mezzo5);
        listaVeicoli.add(mezzo6);

        return listaVeicoli;
    }
}
