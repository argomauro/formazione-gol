package it.gol.modello;

public abstract class ABSVeicolo implements INavigabile{

    protected String modello;
    protected int cilindrata;
    protected int lunghezza;

    public int getLunghezza() {
        return lunghezza;
    }

    public void setLunghezza(int lunghezza) {
        this.lunghezza = lunghezza;
    }

    public ABSVeicolo(String modello, int cilindrata) {
        this.cilindrata = cilindrata;
        this.modello = modello;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public int getCilindrata() {
        return cilindrata;
    }

    public void setCilindrata(int cilindrata) {
        this.cilindrata = cilindrata;
    }

    public abstract void mostraVelocitaMassima();

    public abstract int postiDisponibili();
}

