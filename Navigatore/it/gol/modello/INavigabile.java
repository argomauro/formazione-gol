package it.gol.modello;

public interface INavigabile {
    void calcolaPercorso(String destinazione);
    //void iniziaNavigazione();
}
