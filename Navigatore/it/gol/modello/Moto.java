package it.gol.modello;

public class Moto extends ABSVeicolo {
    private boolean cavalletto = false;
    private boolean isSideCar = false;
    
    public boolean isCavalletto() {
        return cavalletto;
    }

    public void setCavalletto(boolean cavalletto) {
        this.cavalletto = cavalletto;
    }

    public Moto(String modello, int cilindrata) {
        super(modello, cilindrata);
    }

    @Override
    public String toString() {
        return "Moto [modello=" + modello + ", cilindrata=" + cilindrata + "]";
    }

    @Override
    public void mostraVelocitaMassima() {
        System.out.println("Velocità massima di Moto " + modello + "e' 200 kmh");
        
    }

    @Override
    public int postiDisponibili() {
        int posti = 2;
        if(isSideCar)
            posti = 3;
        return posti;
    }

    public boolean isSideCar() {
        return isSideCar;
    }

    public void setSideCar(boolean isSideCar) {
        this.isSideCar = isSideCar;
    }

    @Override
    public void calcolaPercorso(String destinazione) {
        String tempo = "2gg";
        if(cilindrata == 300)
            tempo = "1gg";
        System.out.println("Per raggiungere "+destinazione+ " ci vogliono "+tempo);
        
    }

    
    

    

    
    
}

