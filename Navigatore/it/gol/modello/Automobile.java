package it.gol.modello;

public class Automobile extends ABSVeicolo {
     
    
    public Automobile(String modello, int cilindrata) {
        super(modello, cilindrata);
    }

    @Override
    public String toString() {
        return "Automobile [modello=" + modello + ", cilindrata=" + cilindrata + "]";
    }

    @Override
    public void mostraVelocitaMassima() {
        System.out.println("Velocità massima di Auto " + modello + "e' 100 kmh");
        
    }

    @Override
    public int postiDisponibili() {
        return 5;
    }


    @Override
    public void calcolaPercorso(String destinazione) {
        System.out.println("Per raggiungere "+destinazione+ " ci vogliono 3ore");
        
    }
    
}

