# Esercizio: Sistema di Navigazione per Veicoli

## Contesto
L'obiettivo di questo esercizio è progettare e implementare un sistema di navigazione che possa essere adottato da diversi tipi di veicoli, come automobili, barche e aerei. Utilizzeremo le **interfacce** per definire le funzionalità di navigazione e una **classe astratta** per condividere implementazioni comuni tra i diversi veicoli.

## Parte 1: Interfacce

### Navigabile
Definisce le capacità di navigazione essenziali che ogni veicolo deve implementare.

#### Metodi:
- `calcolaPercorso(String destinazione): void` - Calcola il percorso verso una destinazione specificata.
- `iniziaNavigazione(): void` - Inizia la navigazione verso la destinazione impostata.

## Parte 2: Classe Astratta

### Veicolo
Una classe astratta che fornisce alcune implementazioni di base comuni a tutti i veicoli.

#### Attributi:
- `marca` (String) - Marca del veicolo.
- `modello` (String) - Modello del veicolo.

#### Metodi:
- Costruttore che inizializza `marca` e `modello`.
- Metodo astratto `mostraVelocitaMassima()` - Ogni tipo di veicolo deve fornire una propria implementazione di questo metodo, mostrando la sua velocità massima.

## Parte 3: Implementazione

### Automobile
Estende `Veicolo` e implementa `Navigabile`. Offre implementazioni specifiche per `calcolaPercorso` e `iniziaNavigazione`, oltre a definire `mostraVelocitaMassima` per un'automobile.

### Barca
Estende `Veicolo` e implementa `Navigabile`. Fornisce implementazioni adatte alla navigazione in acqua per `calcolaPercorso` e `iniziaNavigazione`, e specifica `mostraVelocitaMassima` per una barca.

## Parte 4: Classe di Test

### TestNavigazione
Classe con un metodo `main` che dimostra l'uso delle classi `Automobile` e `Barca`. Crea istanze di entrambi i tipi di veicoli e utilizza i loro metodi per mostrare come gestiscono la navigazione e visualizzano la loro velocità massima.

## Obiettivi dell'Esercizio

- **Comprendere le Interfacce:** Gli studenti impareranno come le interfacce possono essere utilizzate per definire un set di metodi che diverse classi devono implementare.
- **Utilizzo delle Classi Astratte:** Vedranno come una classe astratta può fornire implementazioni di base che altre classi possono ereditare.
- **Differenze tra Interfacce e Classi Astratte:** Questo esercizio chiarisce i casi d'uso e le differenze tra interfacce e classi astratte in Java.
