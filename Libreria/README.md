# Esercizio: Gestione di una Biblioteca

## Contesto
Lo scopo dell'esercizio è sviluppare un sistema semplice per la gestione di una biblioteca. Questo sistema permetterà di tenere traccia dei libri disponibili in biblioteca, dei prestiti effettuati agli utenti e della gestione degli utenti stessi.

## Classi da Implementare

### Libro

#### Attributi:
- Titolo (String)
- Autore (String)
- Anno di pubblicazione (int)
- Codice univoco (String)

#### Metodi:
- Costruttore per inizializzare tutti gli attributi
- Metodi get/set per ogni attributo

#### Extra:
- Override del metodo `toString()` per stampare le informazioni del libro in modo leggibile.

### Utente

#### Attributi:
- Nome (String)
- Cognome (String)
- ID Utente (String)
- Elenco dei libri presi in prestito (ArrayList<Libro>)

#### Metodi:
- Costruttore per inizializzare nome, cognome e ID. Inizializza anche l'elenco dei libri presi in prestito come vuoto.
- Metodi get/set per nome, cognome e ID.
- Metodo per aggiungere un libro all'elenco dei prestiti.
- Metodo per rimuovere un libro dall'elenco dei prestiti.

#### Extra:
- Override del metodo `toString()` per stampare le informazioni dell'utente.

### Biblioteca

#### Attributi:
- Elenco dei libri disponibili (ArrayList<Libro>)
- Elenco degli utenti registrati (ArrayList<Utente>)

#### Metodi:
- Costruttore per inizializzare le liste come vuote.
- Metodo per aggiungere un libro all'elenco dei libri disponibili.
- Metodo per registrare un nuovo utente.
- Metodo per effettuare un prestito (deve verificare la disponibilità del libro e aggiornare l'elenco dei libri dell'utente).
- Metodo per ricevere un libro restituito (aggiorna sia l'elenco dei libri disponibili sia l'elenco dei libri dell'utente).

## Classe di Controllo
Creare una classe `GestioneBiblioteca` con un metodo `main` per testare tutte le funzionalità delle classi sopra descritte. Questo può includere:
- Creazione di vari oggetti `Libro` e `Utente`.
- Registrazione degli utenti nella biblioteca.
- Aggiunta di libri alla collezione della biblioteca.
- Prestiti e restituzioni di libri.

Questo esercizio permette di coprire vari aspetti della programmazione orientata agli oggetti, come la creazione di classi e oggetti, l'uso di metodi get/set, costruttori, variabili di istanza vs variabili statiche (potresti, per esempio, avere un contatore statico per tenere traccia del numero totale di libri o utenti), e le relazioni tra oggetti.