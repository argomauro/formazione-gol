package it.gol.controllo;

import java.util.ArrayList;

import it.gol.modello.Libro;
import it.gol.modello.Utente;

public class Biblioteca {
    private ArrayList<Libro> libriDisponibili;
    private ArrayList<Utente> utentiRegistrati;

    public Biblioteca() {
        this.libriDisponibili = new ArrayList<>();
        this.utentiRegistrati = new ArrayList<>();
    }

    public void aggiungiLibro(Libro libro) {
        libriDisponibili.add(libro);
    }

    public void registraUtente(Utente utente) {
        utentiRegistrati.add(utente);
    }

    public boolean effettuaPrestito(Libro libro, Utente utente) {
        if (libriDisponibili.contains(libro)) {
            utente.aggiungiLibroPrestito(libro);
            libriDisponibili.remove(libro);
            return true;
        } else {
            return false;
        }
    }

    public boolean riceviRestituzione(Libro libro, Utente utente) {
        if (!libriDisponibili.contains(libro)) {
            utente.rimuoviLibroPrestito(libro);
            libriDisponibili.add(libro);
            return true;
        } else {
            return false;
        }
    }
}
