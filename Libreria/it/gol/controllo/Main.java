package it.gol.controllo;

import it.gol.modello.Libro;
import it.gol.modello.Utente;

public class Main {
    public static void main(String[] args) {
        Biblioteca biblioteca = new Biblioteca();
        Utente utente = new Utente("Mario", "Rossi", "ID001");
        Libro libro = new Libro("Il Signore degli Anelli", "J.R.R. Tolkien", 1954, "COD123");

        biblioteca.registraUtente(utente);
        biblioteca.aggiungiLibro(libro);

        System.out.println("Effettuo il prestito del libro all'utente...");
        biblioteca.effettuaPrestito(libro, utente);

        System.out.println("Stato attuale dell'utente:");
        System.out.println(utente);

        System.out.println("\nRicevo la restituzione del libro...");
        biblioteca.riceviRestituzione(libro, utente);

        System.out.println("Stato attuale dell'utente dopo la restituzione:");
        System.out.println(utente);
    }
}
