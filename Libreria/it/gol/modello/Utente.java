package it.gol.modello;

import java.util.ArrayList;

public class Utente {
    private String nome;
    private String cognome;
    private String idUtente;
    private ArrayList<Libro> libriPrestito;

    public Utente(String nome, String cognome, String idUtente) {
        this.nome = nome;
        this.cognome = cognome;
        this.idUtente = idUtente;
        this.libriPrestito = new ArrayList<>();
    }

    // Metodi get e set
    public String getNome() { return nome; }
    public String getCognome() { return cognome; }
    public String getIdUtente() { return idUtente; }

    public void aggiungiLibroPrestito(Libro libro) {
        libriPrestito.add(libro);
    }

    public void rimuoviLibroPrestito(Libro libro) {
        libriPrestito.remove(libro);
    }

    @Override
    public String toString() {
        return "Utente{" +
                "nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                ", idUtente='" + idUtente + '\'' +
                ", libriPrestito=" + libriPrestito +
                '}';
    }
}

