package it.gol.modello;

public class Libro {
    private String titolo;
    private String autore;
    private int annoPubblicazione;
    private String codice;

    public Libro(String titolo, String autore, int annoPubblicazione, String codice) {
        this.titolo = titolo;
        this.autore = autore;
        this.annoPubblicazione = annoPubblicazione;
        this.codice = codice;
    }

    // Metodi get e set
    public String getTitolo() { return titolo; }
    public String getAutore() { return autore; }
    public int getAnnoPubblicazione() { return annoPubblicazione; }
    public String getCodice() { return codice; }

    public void setTitolo(String titolo) { this.titolo = titolo; }
    public void setAutore(String autore) { this.autore = autore; }
    public void setAnnoPubblicazione(int annoPubblicazione) { this.annoPubblicazione = annoPubblicazione; }
    public void setCodice(String codice) { this.codice = codice; }

    @Override
    public String toString() {
        return "Libro{" +
                "titolo='" + titolo + '\'' +
                ", autore='" + autore + '\'' +
                ", annoPubblicazione=" + annoPubblicazione +
                ", codice='" + codice + '\'' +
                '}';
    }
}
