# Esercizi di Programmazione Java

## Indice

1. [Analizzatore di Testi](#analizzatore-di-testi)
2. [Gestione di una Libreria](#gestione-di-una-libreria)
3. [Sistema di Navigazione per Veicoli](#sistema-di-navigazione-per-veicoli)
4. [Gestione delle Principali Strutture Dati](#gestione-delle-principali-strutture-dati)

---

## Analizzatore di Testi

### Obiettivo
Sviluppare un'applicazione Java per eseguire varie operazioni di analisi e manipolazione su stringhe di testo.

### Funzionalità
- Contare le occorrenze di una parola nel testo.
- Ricerca della presenza di una parola nel testo.
- Sostituzione di parole nel testo.
- Conversione del testo in maiuscolo o minuscolo.
- Conteggio delle parole presenti nel testo.
- Inversione del testo.

### Skills
Manipolazione delle stringhe, espressioni regolari, uso dei cicli e delle condizioni.

---

## Gestione di una Libreria

### Obiettivo
Creare un sistema per la gestione di una libreria che permetta di tenere traccia dei libri disponibili e dei prestiti agli utenti.

### Classi Principali
- `Libro`: Rappresenta un libro con titolo, autore, e anno di pubblicazione.
- `Utente`: Gestisce gli utenti della libreria.
- `Biblioteca`: Contiene le funzionalità per aggiungere libri, registrare utenti, prestare e ricevere libri.

### Skills
OOP, gestione delle relazioni tra oggetti, ArrayList, HashMap.

---

## Sistema di Navigazione per Veicoli

### Obiettivo
Implementare un sistema di navigazione che possa essere utilizzato da diversi tipi di veicoli, sfruttando le interfacce e le classi astratte.

### Componenti
- Interfaccia `Navigabile`: Definisce le azioni di navigazione.
- Classe astratta `Veicolo`: Fornisce attributi e metodi base per i veicoli.
- Classi `Automobile` e `Barca`: Implementano la navigazione specifica per il loro contesto.

### Skills
Uso delle interfacce, classi astratte, ereditarietà, polimorfismo.

---

## Gestione delle Principali Strutture Dati

### Obiettivo
Esercitarsi con le principali strutture dati in Java: array, ArrayList, HashMap, e Set attraverso la gestione di una biblioteca.

### Descrizione
Implementare un sistema che permetta di:
- Aggiungere e rimuovere libri.
- Tenere traccia dei libri prestati.
- Mantenere un insieme unico di autori.
- Categorizzare i libri.

### Skills
Uso di array, ArrayList, HashMap, Set, gestione delle collezioni.

---
