# Gestione delle Eccezioni in Java

## Introduzione alle Eccezioni

Le eccezioni in Java sono eventi che alterano il normale flusso di esecuzione di un programma. Sono usate per indicare che si è verificato un errore durante l'esecuzione del programma. In Java, le eccezioni sono oggetti che vengono lanciati (throw) da un metodo che incontra una situazione anomala e possono essere catturati (catch) da un altro metodo che ha la capacità di gestirli.

## Tipologie di Eccezioni

In Java, le eccezioni si dividono principalmente in due categorie:

1. **Checked Exceptions**: Sono eccezioni che devono essere gestite esplicitamente nel codice. Sono utili quando vuoi assicurarti che una certa condizione di errore sia prevista e gestita adeguatamente. Ad esempio, `IOException`.

2. **Unchecked Exceptions**: Sono eccezioni che non richiedono una gestione esplicita. Sono generalmente errori di programmazione, come gli errori di logica, o condizioni anomale che il programma non dovrebbe cercare di gestire. Ad esempio, `NullPointerException` e `ArrayIndexOutOfBoundsException`.

## Sintassi di Base per la Gestione delle Eccezioni

La gestione delle eccezioni in Java utilizza principalmente tre parole chiave: `try`, `catch`, e `finally`.

```java
try {
    // Blocco di codice che potrebbe generare un'eccezione
} catch (TipoEccezione1 e) {
    // Gestione dell'eccezione TipoEccezione1
} catch (TipoEccezione2 e) {
    // Gestione dell'eccezione TipoEccezione2
} finally {
    // Codice che viene eseguito sempre, sia che si verifichino eccezioni sia che non si verifichino
}
```

## Esercizio 1: Gestione di una `ArithmeticException`

Immagina di avere un semplice metodo che divide due numeri. Questo metodo potrebbe generare un'`ArithmeticException` se il denominatore è zero. Vediamo come gestirlo.

```java
public class Divisione {

    public static int dividi(int numeratore, int denominatore) {
        try {
            return numeratore / denominatore;
        } catch (ArithmeticException e) {
            System.err.println("Errore: tentativo di divisione per zero.");
            return 0; // Restituiamo un valore di default o gestiamo l'errore in altro modo
        }
    }

    public static void main(String[] args) {
        System.out.println(dividi(10, 2)); // Output: 5
        System.out.println(dividi(10, 0)); // Output: Errore: tentativo di divisione per zero.
    }
}
```

## Esercizio 2: Gestione di una `IOException`

Supponiamo di voler leggere il contenuto di un file. Questa operazione può generare un'`IOException` se il file non esiste o se si verificano errori di lettura. Vediamo come gestirla.

```java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LetturaFile {

    public static void leggiFile(String nomeFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(nomeFile))) {
            String linea;
            while ((linea = reader.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (IOException e) {
            System.err.println("Errore nella lettura del file: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        leggiFile("percorso/al/tuo/file.txt");
    }
}
```

In questo esempio, abbiamo anche utilizzato un blocco `try-with-resources` che assicura che le risorse (`BufferedReader` in questo caso) siano chiuse automaticamente dopo l'uso, indipendentemente dal fatto che si verifichino eccezioni o meno.

## Esercizio 3: Uso di `finally`

A volte potresti voler eseguire del codice indipendentemente dal fatto che si verifichino eccezioni o meno, come per chiudere risorse o per pulire. Per questo si usa il blocco `finally`.

In questo esempio, indipendentemente dal fatto che si verifichino o meno eccezioni all'interno del blocco `try`, il blocco `finally` viene sempre eseguito, garantendo che le operazioni di pulizia siano eseguite in ogni caso.

## Esercizio 4: Propagazione delle Eccezioni

A volte potresti voler che un'eccezione venga gestita dal metodo chiamante. In questo caso, puoi propagare l'eccezione utilizzando la parola chiave `throws`.

```java
public class PropagazioneEccezioni {

    public static void metodoRischioso() throws IOException {
        // Supponiamo che questo metodo possa generare IOException
        throw new IOException("Errore di Input/Output");
    }

    public static void chiamaMetodoRischioso() {
        try {
            metodoRischioso();
        } catch (IOException e) {
            System.err.println("Eccezione catturata: " + e.getMessage());
        }finally{
            // Codice eseguito sempre per pulire o liberare risorse
            System.out.println("Pulizia risorse in corso...");
        }
    }

    public static void main(String[] args) {
        chiamaMetodoRischioso();
    }
}
```

In questo esempio, il metodo `metodoRischioso` dichiara che può lanciare un'`IOException`. Il metodo `chiamaMetodoRischioso` chiama `metodoRischioso` e gestisce l'eccezione che potrebbe essere lanciata.


# Esempi Pratici

## Gestione delle Eccezioni con `ArrayList`

### Esempio: Accesso ad un elemento non esistente

Quando tentiamo di accedere a un elemento di un `ArrayList` utilizzando un indice non valido, viene lanciata un'`IndexOutOfBoundsException`. Vediamo come gestirla.

```java
import java.util.ArrayList;
import java.util.List;

public class EccezioneArrayList {

    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        lista.add("Elemento1");
        lista.add("Elemento2");

        try {
            System.out.println(lista.get(2)); // Accesso a un indice non esistente
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Errore: indice non valido.");
        }
    }
}
```

## Gestione delle Eccezioni con `HashMap`

### Esempio: Gestione `NullPointerException`

Quando si tenta di accedere a una mappa con una chiave `null` che non è permessa, potrebbe essere lanciata una `NullPointerException`, a seconda dell'implementazione della mappa.

```java
import java.util.HashMap;
import java.util.Map;

public class EccezioneHashMap {

    public static void main(String[] args) {
        Map<String, String> mappa = new HashMap<>();
        mappa.put("chiave1", "valore1");

        try {
            String valore = mappa.get(null); // Accesso con chiave null
            System.out.println(valore);
        } catch (NullPointerException e) {
            System.err.println("Errore: chiave null non permessa.");
        }
    }
}
```

## Gestione delle Eccezioni nelle Operazioni con le Stringhe

### Esempio: Conversione di una Stringa in un Numero

Convertire una stringa in un numero con `Integer.parseInt` può generare un `NumberFormatException` se la stringa non contiene un numero valido.

```java
public class EccezioneStringhe {

    public static void main(String[] args) {
        String numeroStr = "abc";

        try {
            int numero = Integer.parseInt(numeroStr); // Conversione fallita
            System.out.println(numero);
        } catch (NumberFormatException e) {
            System.err.println("Errore: la stringa non è un numero valido.");
        }
    }
}
```

## Conclusioni
La gestione delle eccezioni in Java è un aspetto cruciale della scrittura di software affidabile e robusto. Utilizzando `try`, `catch`, `finally`, e `throws`, puoi scrivere codice che gestisce in modo efficace le condizioni di errore, assicurando che il tuo programma si comporti in modo prevedibile anche di fronte a situazioni impreviste.
